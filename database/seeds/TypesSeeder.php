<?php

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['label' => 'event', 'description' => 'collect datas about events'],
            ['label' => 'person', 'description' => 'collect datas about persons'],
            ['label' => 'land', 'description' => 'collect datas about lands'],
        ];

        foreach( $datas as $data )
        {
            $type = new Type($data);
            $type->save();
        }
    }
}
