<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SpiderRequest;
use App\Models\Spider;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SpiderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SpiderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Spider');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/spider');
        $this->crud->setEntityNameStrings('spider', 'spiders');
        $this->crud->addButtonFromView('line', 'manage', 'manage', 'end');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text'
        ]);
        $this->crud->addColumn([
            'name' => 'target',
            'label' => "Target",
            'type' => 'text'
        ]);
        $this->crud->addColumn([
            'label' => "Type",
            'type' => "select",
            'name' => 'type_id', // the column that contains the ID of that connected entity;
            'entity' => 'type', // the method that defines the relationship in your Model
            'attribute' => "label", // foreign key attribute that is shown to user
            'model' => "App\Models\Type", // foreign key model
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SpiderRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'target',
            'label' => "Target",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
        ]);
        $this->crud->addField([
            'label' => "Type",
            'type' => 'select',
            'name' => 'type_id', // the db column for the foreign key
            'entity' => 'type', // the method that defines the relationship in your Model
            'attribute' => 'label', // foreign key attribute that is shown to user
            'model' => "App\Models\Type",
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }

    public function manage($id)
    {
        $spider = Spider::find($id);
       return view('admin/spiders/manage', ['crud' => $this->crud, 'spider' => $spider]);
    }
}
