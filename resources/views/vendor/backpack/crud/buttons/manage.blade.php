@if ($crud->hasAccess('update'))
    <a href="{{ url($crud->route.'/'.$entry->getKey().'/manage') }} " class="btn btn-xs btn-default"><i class="fa fa-gear"></i> Manage</a>
@endif
