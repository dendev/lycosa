<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<hr/>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('home') }}"><i class="fa fa-home nav-icon"></i> Site</a></li>
<hr/>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('spider') }}'><i class='nav-icon fa fa-question'></i> Spiders</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('type') }}'><i class='nav-icon fa fa-question'></i> Types</a></li>
<hr/>
<li class=nav-item><a class=nav-link href="{{ backpack_url('elfinder') }}"><i class="nav-icon fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
