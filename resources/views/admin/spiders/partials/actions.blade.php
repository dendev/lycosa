<div class="card">
    <div class="card-body">
        <h5 class="card-title">Actions</h5>
        <!-- Actions Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" id="help-tab" data-toggle="tab" href="#help-content" role="tab" aria-controls="help" aria-selected="false">Help</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="all-tab" data-toggle="tab" href="#all-content" role="tab" aria-controls="all" aria-selected="false">Get All</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="one-tab" data-toggle="tab" href="#one-content" role="tab" aria-controls="one" aria-selected="false">Get One</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="debug-tab" data-toggle="tab" href="#debug-content" role="tab" aria-controls="debug" aria-selected="false">Debug</a>
            </li>
        </ul>

        <!-- Actions Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="help-content" role="tabpanel" aria-labelledby="help-tab">
                @include('admin.partials.loader')
            </div>
            <div class="tab-pane" id="all-content" role="tabpanel" aria-labelledby="all-tab">
                @include('admin.partials.loader')
            </div>
            <div class="tab-pane" id="one-content" role="tabpanel" aria-labelledby="one-tab">
                <form>
                    <div class="form-group">
                        <label for="url">Url</label>
                        <input type="text" class="form-control" id="get-one-url-value" placeholder="Url...">
                        <button type="button" class="btn btn-primary mt-2" id="one-btn">Ok</button>
                    </div>
                </form>
                @include('admin.partials.loader', ['show' => false])
            </div>
            <div class="tab-pane" id="debug-content" role="tabpanel" aria-labelledby="debug-tab">
                <form>
                    <div class="form-group">
                        <label for="url">Url</label>
                        <input type="text" class="form-control" id="debug-url-value" placeholder="Url...">
                        <button type="button" class="btn btn-primary mt-2" id="debug-btn">Ok</button>
                    </div>
                </form>
                @include('admin.partials.loader', ['show' => false])
            </div>
        </div>
    </div>
</div>
