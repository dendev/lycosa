<div class="card">
    <div class="card-body">
        <h5 class="card-title">States</h5>
        <!-- States Nav tabs -->
        <ul class="nav nav-tabs" id="statesTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="status-tab" data-toggle="tab" href="#status-content" role="tab" aria-controls="status" aria-selected="true">Status</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="ping-tab" data-toggle="tab" href="#ping-content" role="tab" aria-controls="ping" aria-selected="false">Ping</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="errors-tab" data-toggle="tab" href="#errors-content" role="tab" aria-controls="errors" aria-selected="false">Errors</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="logs-tab" data-toggle="tab" href="#logs-content" role="tab" aria-controls="logs" aria-selected="false">Logs</a>
            </li>
        </ul>

        <!-- States Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="status-content" role="tabpanel" aria-labelledby="status-tab">
                @include('admin.partials.loader')
            </div>
            <div class="tab-pane" id="ping-content" role="tabpanel" aria-labelledby="ping-tab">
                @include('admin.partials.loader')
            </div>
            <div class="tab-pane" id="errors-content" role="tabpanel" aria-labelledby="errors-tab">
                @include('admin.partials.loader')
                @include('admin.partials.purgelogs')
            </div>
            <div class="tab-pane" id="logs-content" role="tabpanel" aria-labelledby="logs-tab">
                @include('admin.partials.loader')
                @include('admin.partials.purgelogs')
            </div>
        </div>
    </div>
</div>

