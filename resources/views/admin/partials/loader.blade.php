<?php
$show = isset($show) ? $show : true;
?>
<div class="loader" id="loader-squares" @if( ! $show ) style="display: none" ; @endif>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>
