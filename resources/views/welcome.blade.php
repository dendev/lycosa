<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="96x96" href="/images/spider.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js', env('ENABLE_HTTPS', false)) }}"></script>
    @yield('scripts')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css', env('ENABLE_HTTPS', false)) }}" rel="stylesheet">
</head>
<body>
<div class="row">
    <div class="col-md-4">
        <div class="t1 text-white">
            <i><a href="/admin">Lycosa</a> is a genus of wolf spiders <br>distributed throughout most of the world. <br><br>
                Often called the "true tarantula", <br><br>
                Lycosa spp. can be distinguished <br>from common wolf spiders by their relatively large size</i>
        </div>
    </div>
    <div class="col-md-4 mt-5">
        <img src="{{asset('images/spider-bg.jpeg')}}">
    </div>
    <div class="col-md-4">
        <div class="t2 text-white">
            <h1>Et qu'un peuple muet<br> d'infâmes araignées<br>
                Vient tendre ses filets<br> au fond de nos cerveaux,</h1>
        </div>
    </div>
</div>
</body>
</html>

