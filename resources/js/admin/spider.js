const axios = require('axios');

// initial state
$('#status-tab').ready(status({data: {callback:set_status_response}}));
$('#help-tab').ready(help({data: {callback:set_help_response}}));

// set config
const config = [
    {id: '#status-tab', event: 'click', action: status, set_response: set_status_response},
    {id: '#ping-tab', event: 'click', action: ping, set_response: set_ping_response},
    {id: '#errors-tab', event: 'click', action: errors, set_response: set_errors_response},
    {id: '#logs-tab', event: 'click', action: logs, set_response: set_logs_response},
    {id: '.purgelogs-btn', event: 'click', action: purge_logs, set_response: set_purge_logs_response},
    {id: '#help-tab', event: 'click', action: help, set_response: set_help_response},
    {id: '#all-tab', event: 'click', action: get_all, set_response: set_get_all_response},
    {id: '#one-btn', event: 'click', action: get_one, set_response: set_get_one_response},
    {id: '#debug-btn', event: 'click', action: get_debug, set_response: set_debug_response},
    ];

// action for event
for(let i = 0; i < config.length; i++)
{
    let id = config[i].id;
    let event = config[i].event;
    let action = config[i].action;
    let callback = config[i].set_response;

    // Equivalent event setup using the `.on()` method
    $( id ).on( event, { callback: callback}, action );
}


// call to actions
function status(event)
{
    generic_action(event, 'status');
}

function ping(event)
{
    generic_action(event, 'ping');
}

function errors(event)
{
    generic_action(event, 'errors');
}

function logs(event)
{
    generic_action(event, 'logs');
}

function purge_logs(event)
{
    generic_action(event, 'purge_logs');
}

function help(event)
{
    generic_action(event, 'help');
}

function get_one(event)
{
    let url = $('#get-one-url-value').val();

    $('#one-content > form').hide();
    $('#one-content > #loader-squares').show();

    generic_action(event, 'get_one', url);
}

function get_debug(event)
{
    let url = $('#debug-url-value').val();

    $('#debug-content > form').hide();
    $('#debug-content > #loader-squares').show();

    generic_action(event, 'get_one', url);
}

function get_all(event)
{
    generic_action(event, 'get_all');
}

function generic_action(event, action, arg)
{
    arg = ( typeof arg === 'undefined' ) ? null : arg;
    axios.post('http://localhost:3333', { // TODO hot in config !
        action: action,
        arg: arg
    },{
    })
        .then(function (response) {
            console.log(response);
            event.data.callback(response.data.data, response.status );
        })
        .catch(function (error) {
            set_error_alert( error );
        });
}

//
// - Response
//
function set_ping_response(datas, http_code)
{
    let id_content = '#ping-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    let html = `
        <div class="alert alert-success text-center" role="alert">${datas[0]}</div>
    `;

    $(id_content).empty();
    $(id_content).html(html);
}

function set_status_response(datas, http_code)
{
    let id_content = '#status-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    let online = datas.online;
    let nb_erros = datas.nb_errors;
    let uptime = datas.uptime;
    let html = `
        <div class="alert alert-${(online ? 'success' : 'warning')} text-center" role="alert">${(online) ? 'Online' : 'Offline'}</div>
        <div class="alert alert-${(nb_erros === 0) ? 'success' : 'danger'} text-center" role="alert">${nb_erros} Errors</div>
        <div class="alert alert-info text-center" role="alert">${uptime} Up</div>
    `;

    $(id_content).empty();
    $(id_content).html(html);
}

function set_get_one_response(datas, http_code)
{
    let id_content = '#one-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    let html = _make_html_event(datas);

    $(id_content).empty();
    $(id_content).html(html);
}

function set_debug_response(datas, http_code)
{
    let id_content = '#debug-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    let html = _make_json_event(datas);

    $(id_content).empty();
    $(id_content).html(html);
}

function set_get_all_response(datas, http_code)
{
    let id_content = '#all-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    // save datas && fct for onchange event in pagination
    window.events = datas;
    window.update_event_html = function(id_number, action){
        console.log( action );

        // set current event to display
        let display_value = $(`#${id_number}`).val();
        let real_value = display_value - 1;

        console.log( real_value );
        if( action === 'prev' )
        {
            if( ( real_value - 1 ) >= 0 )
            {
                real_value = real_value - 1;
                display_value = real_value + 1;
                $(`#${id_number}`).val(display_value);
            }
        }
        else if( action === 'next' )
        {
            if( ( real_value + 1 ) < window.events.length )
            {
                real_value = real_value + 1;
                display_value = real_value + 1;
                $(`#${id_number}`).val(display_value);
            }
        }

        // display event
        let content = _make_html_event(window.events[real_value]);
        $(id_content + ' > #events_viewer').html(content);
    };

    // clean
    $(id_content).empty();

    // first event + root div
    let current = 0;

    // pagination
    let pagination = _make_html_pagination(current, datas);
    $(id_content).append(pagination);

    // content of event
    let content = _make_html_event(datas[current]);
    content = `<div id='events_viewer'>${content}</div>`;
    $(id_content).append(content);
}

function set_errors_response(datas, http_code)
{
    let id_content = '#errors-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    _logs_data_to_html(datas, id_content)
}

function set_logs_response(datas, http_code)
{
    let id_content = '#logs-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    _logs_data_to_html(datas, id_content)
}

function set_purge_logs_response(datas, http_code)
{
    let id_logs_content = '#logs-content';
    let id_errors_content = '#errors-content';

    $(id_logs_content).find('.alert').remove();
    $(id_logs_content).find('#loader-squares').remove();

    $(id_errors_content).find('.alert').remove();
    $(id_errors_content).find('#loader-squares').remove();
}

function set_help_response(datas, http_code)
{
    let id_content = '#help-content';

    if( http_code !== 200 )
        return set_no_datas_response(id_content);

    $(id_content).empty();
    for( let i = 0; i < datas.length; i++ )
    {
        let data = datas[i];
        let html = `<b>${data.action} </b>( args: ${(data.args) ? data.args : ''})<br><i>${data.description}</i><br><br>`;
        $(id_content).append(html);
    }
}

//
// - MSG
//
function set_error_alert(error)
{
    console.log( 'NET ERROR' );
    let boxes = [
        '#status-content',
        '#ping-content',
        '#errors-content',
        '#logs-content',
        '#help-content',
        '#all-content',
        '#one-content',
    ];

    let msg = ( error.constructor === Object ) ? error.message : error;
    let html = `<div class="alert alert-danger text-center" role="alert">${msg}!</div>`;

    for( let i = 0; i < boxes.length; i++)
    {
        let id_box = boxes[i];
        $(id_box).empty();
        $(id_box).html(html);
    }
}

function set_no_datas_response(id_content)
{
    let html = `
        <div class="row">
            <div class="col-md-11">
                <div class="alert alert-warning text-center" role="alert">No datas found!</div>
            </div>
            <div class="col-md-1">
                <button type="button" class="btn btn-primary refresh" style="margin-top: 6px;" onclick="location.reload();"><i class="fa fa-refresh"></i></button>
            </div>
        </div>
        `;

    $(id_content).empty();
    $(id_content).html(html);
}

//
// - Private
//
function _logs_data_to_html(datas, id_content)
{
    datas = datas.reverse();

    $(id_content).find('.alert').remove();
    $(id_content).find('#loader-squares').remove();
    $(id_content).find('.purgelogs').show();

    for (let i = 0; i < datas.length; i++) {
        let data = JSON.parse(datas[i]);
        let msg = data.message;
        let type = _log_level_to_alert_type(data.level);
        let timestamp = data.timestamp;

        let html = `<div class="alert alert-${type}" role="alert"><span>${msg}</span><span class="align-right">${timestamp}</span></div>`;

        $(`id_content, .purgelogs`).prepend(html);
    }
}

function _log_level_to_alert_type(level)
{
    let type;

    switch (level) {
        case 'emerg':
            type = 'danger';
            break;
        case 'alert':
            type = 'danger';
            break;
        case 'error':
            type = 'danger';
            break;
        case 'warn':
            type = 'warning';
            break;
        case 'notice':
            type = 'success';
            break;
        case 'info':
            type = 'primary'
            break;
        case 'debug':
            type = 'secondary';
            break;
    }

    return type;
}

function _make_html_event(event_datas)
{
    let src_url = event_datas['src_url'];
    let title = event_datas['title'];
    let description = event_datas['description'];

    let begin_at = new Date( Date.parse( event_datas['begin_at'] ) );
    let end_at = new Date( Date.parse( event_datas['end_at'] ) ) ;
    let date_full = `${begin_at.toLocaleDateString()} ${begin_at.toLocaleTimeString()} - ${end_at.toLocaleDateString()} ${end_at.toLocaleTimeString()}`;

    let address_street = event_datas['address']['street'];
    let address_cp = event_datas['address']['cp'];
    let address_locality = event_datas['address']['locality'];
    let address_country = event_datas['address']['country'];
    let address_full = `${address_street}, ${address_cp} - ${address_locality} (${address_country})`;

    let price = event_datas['price'];
    let image = event_datas['image'];

    let html = `
        <h3>${title}</h3>
        <div class="card">
          <div class="card-body">
            <p class="card-text"><i class="fa fa-clock-o"> ${date_full}</i></p>
            <p class="card-text"><i class="fa fa-map-marker"> ${address_full}</i></p>
            <p class="card-text"><i class="fa fa-euro"> ${price}</i></p>
          </div>
        </div>
        <img src="${image}">
        <br>
        <p>${description}</p>
        <br>
        <a class="btn btn-secondary" href="${src_url}" target="_blank" role="button">Source</a>
        <button type="button" class="btn btn-primary refresh" onclick="location.reload();"><i class="fa fa-refresh"></i></button>
   `;

    return html;
}

function _make_json_event(event_datas)
{
    let src_url = event_datas['src_url'];

    let content = '';
    for( let key in event_datas )
    {
        content += `<b>${key} : </b><pre>${_escapeHtml(JSON.stringify(event_datas[key]))}</pre>`
    }
    let html = `
        ${content}
        <a class="btn btn-secondary" href="${src_url}" target="_blank" role="button">Source</a>
        <button type="button" class="btn btn-primary refresh" onclick="location.reload();"><i class="fa fa-refresh"></i></button>
        `;

    return html;
}

function _make_html_pagination(current, datas)
{
    let total = datas.length;
    current++;
    let html = `
    <div class="mt-2 mb-2" align="center">
        <button type="button" class="btn btn-primary float-left" onclick="window.update_event_html('event_number', 'prev')"><i class="fa fa-angle-left"></i></button>
        <button type="button" class="btn btn-secondary"><input type="text" size="4" name="event_number" id="event_number" value="${current}" onchange="window.update_event_html('event_number', 'set')"></input> / ${total}</button>
        <button type="button" class="btn btn-primary float-right" onclick="window.update_event_html('event_number', 'next')"><i class="fa fa-angle-right"></i></button>
    </div>
    `;
    return html;
}

function _escapeHtml(unsafe) {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}
